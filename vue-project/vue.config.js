const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;
  const path = require("path");
module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "./" : "/",
  configureWebpack: {
    plugins: [new BundleAnalyzerPlugin()]
  },
  outputDir: path.resolve(__dirname, "../../../BITBUCKET/guide/resources/views/guide_project"),
  assetsDir: "guide-project",
  runtimeCompiler: true
};
