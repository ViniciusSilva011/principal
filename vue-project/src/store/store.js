import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex)
import guides from './modules/guides/guides';

export default new Vuex.Store({
    modules: { guides }
});