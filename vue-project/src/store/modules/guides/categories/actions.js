import axios from 'axios';
export default {
    deleteCat({ commit }, { idCat }) {
        axios.delete(`/api/categories/${idCat}`)
            .then(({ data }) => {
                // console.log('data :', data);
                // debugger
                commit('deleteFromData', idCat);
                return data;
            });

    }
}