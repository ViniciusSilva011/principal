//* categories
import Vue from 'vue';
export default {
    addCategory({ data }, payload) {
        data.push(payload);
    },
    setData(state, payload) {
        Vue.set(state, 'data', payload);
    },
    addCategoryToVisibles({ visibles }, category) {
        visibles.push(category);
    },
    deleteFromData({ data }, id) {
        let index = data.findIndex(e => e.id === id);
        data.splice(index, 1);
        // console.log('index :', index);
    }

}
//! _.remove();