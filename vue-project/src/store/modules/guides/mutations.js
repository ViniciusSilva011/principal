import Vue from 'vue';

export default {
    setPages(state, payload) {
        Vue.set(state, 'pages', payload);
    },
    addToPages({ pages }, payload) {
        pages.push(payload);
    },
    removeFromPages({ pages }, pageId) {
        let index = this.findIndex(e => pageId === e.id);
        pages.splice(index, 1);
    },
    setActivePage(state, payload) {
        Vue.set(state, 'activePage', payload);
    },
    setActiveTabForFields(state, payload) {
        Vue.set(state, 'activeTabForFields', payload);
    },
    setActiveField(state, payload) {
        Vue.set(state, 'activeField', payload);
    },
    addToBreadcrumb({ breadcrumb }, payload) {
        breadcrumb.push(payload);
    },
    setBreadcrumb(state, payload) {
        Vue.set(state, 'breadcrumb', payload);
    },
}