export default {
    getPages: state => state.pages,
    getActivePage: state => state.activePage,
    getBreadcrumb: state => state.breadcrumb,
    getForms: state => state.forms,
    getActiveField: state => state.activeField,
    getActiveTabForFields: state => state.activeTabForFields
}
