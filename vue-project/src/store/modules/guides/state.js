export default {
    pages: [],
    activePage: {
        id: 0,
        name: "",
        builder: [{
            title: "",
            fields: []
        }],
        guide_id: null,
        updated_at: "",
        created_at: "",
        visible: false,
        temp: {
            deletedImages: [],
            imagesFileToUpload: [],
            imagesToUpdate: [] // { nameImageInFormData: "image_"+imagesQtt,uploadField:this.currentField }
        },
        categories: []
    },
    forms: {
        title: "",
        fields: []
    },
    breadcrumb: [],
    activeField: {},
    activeTabForFields: "",
    server: process.env.NODE_ENV === "production" ? 'https://guidedev.floow.net:443' : 'http://192.168.200.207:8000'//esqueci -> Não
    // server: 'http://192.168.200.40:8000'//esqueci -> ??
}