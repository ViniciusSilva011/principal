import Vue from "vue";
import store from "@/store/store";

// ================
//* Use Element UI
// ----------------

import Element from "element-ui";
// import "element-ui/lib/theme-chalk/index.css";
import "./assets/flow-style/theme/index.css"
import locale from "element-ui/lib/locale/lang/en"; // Default lang is Chinese
import 'element-ui/lib/theme-chalk/display.css';

Vue.use(Element, { locale });
import "./assets/scss/main.scss";
// ================
//* Lodash
// ----------------
import VueLodash from "vue-lodash";
Vue.use(VueLodash);

// ================
//* NProgress
// ----------------
import './plugins/nprogress'

// ================
//* Vue Router
// ----------------
import router from "./router";
import './plugins/axios'
import App from "./App";

Vue.config.productionTip = false;

var vm = new Vue({
    el: "#app",
    router,
    store,
    render: h => h(App)
}).$mount("#app");
export default vm;
