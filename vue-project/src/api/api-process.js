import axios from 'axios';
import store from '@/store/store';



export const verifyConditions = (id) => {
    return new Promise((resolve) => {
        let data = store.getters['guides/getPages'];
        if (!data.length) {
            getData()
                .then((data) => {
                    return checkConditions(id, data);
                }).then(resolved => {
                    resolve(resolved);
                });
        }
        else {
            checkConditions(id, data).then((resolved) => {
                resolve(resolved);
            })
        }
    })
}
/**
 * 
 * @param { String } id receive id from process/page
 * @param { Array } data receive processes array
 */
export const checkConditions = (id, data) => {
    return new Promise((resolve) => {
        let activePageAp = _.assign(_.cloneDeep(_.find(data, e => id == e.id)),
            { temp: { deleteImagesFromServer: [], imagesFileToUpload: [], imagesToUpdate: [] } });
        if (
            !activePageAp.hasOwnProperty('id')
        ) {
            resolve({ pages: data, isUndefined: true });
        } else {
            store.commit('guides/setPages', data);
            store.commit('guides/setActivePage', activePageAp);
            store.commit('guides/setBreadcrumb', []);
            store.commit('guides/addToBreadcrumb', { id: activePageAp.id, name: activePageAp.name });
            resolve('Ok');
        }
    })
}

/**
 * @return { Promise } return all DB data with first element
 */
export const insertFirstTreeElement = function () {
    return new Promise((resolve, reject) => {
        axios
            .post("/api/guides", {
                id: null,
                name: "First Tree Element",
                builder: [{
                    title: "",
                    fields: []
                }],
                visible: false,
                guide_id: null,
                updated_at: '1900',
                created_at: '1900'
            })
            .then(({ data }) => {
                resolve(data);
            })
            .catch((res) => {
                reject(res);
            });
    });
};

export const getData = () => {
    return new Promise((resolve) => {
        axios.get("/api/guides")
            .then(({ data }) => {
                if (!data) {
                    insertFirstTreeElement()
                        .then((page) => {
                            page = _.assign(page,
                                {
                                    temp: {
                                        deleteImagesFromServer: [],
                                        imagesFileToUpload: [],
                                        imagesToUpdate: []
                                    }
                                });
                            let pages = [];
                            pages.push(page);
                            store.commit('guides/setPages', pages);
                            resolve(pages);
                        });
                }
                else {
                    store.commit('guides/setPages', data.guides);
                    store.commit('guides/categories/setData', data.visibleCategories);
                    resolve(data.guides);
                }
            });
    })

}
