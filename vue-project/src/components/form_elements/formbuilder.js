// #region Imports

import Vue from "vue";
import draggable from "vuedraggable";

// import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
// import { faUserSecret, faUndo, faRedo, faBold, faItalic, faStrikethrough, faUnderline, faCode, faParagraph,
// faListUl, faListOl, faQuoteRight, faChevronLeft,faChevronRight } from '@fortawesome/free-solid-svg-icons'
// library.add(faUserSecret, faUndo, faRedo, faBold, faItalic, faStrikethrough, faUnderline, faCode, faParagraph,
//     faListUl, faListOl, faQuoteRight, faChevronLeft,faChevronRight)
Vue.component('font-awesome-icon', FontAwesomeIcon)



import TitleEditor from "./FormElementTitleEditor";
import Button from "./FormElementButton";
import TiptapComponent from "./TiptapComponent";
import HistoryBar from './HistoryBar';
import Upload from './Upload';

import Elements from "./Elements";
import Properties from "./properties/Properties";


import vm from "../../main";
//#endregion

export const FormBuilder = new Vue({
    components: {
        TitleEditor,
        Upload,
        Button,
        TiptapComponent,
        HistoryBar,
        Elements,
        Properties,
        draggable,
    },
    data() {
        return {
            fields: [
                {
                    fieldType: "TitleEditor",
                    label: "Title",
                    text: "Title",
                    group: "form", //form group
                    isHelpBlockVisible: false,
                    isPlaceholderVisible: true,
                    isUnique: false,
                    span: 24,
                    content: null

                },
                {
                    fieldType: "TiptapComponent",
                    text: "Description",
                    group: "static",
                    content: null,
                    isUnique: false,
                    span: 18,
                    advancedOptions: "",
                    images: [],
                    files: []
                },
                {
                    fieldType: "Button",
                    text: "Button",
                    group: "button",
                    height: 4,
                    transparent: false,
                    fontSize: 6,
                    link: false,
                    buttonText: "Hi!",
                    isHelpBlockVisible: false,
                    isPlaceholderVisible: false,
                    isUnique: false,
                    span: 3,
                    labelWidth: 100,
                    typeOfLinkToPage: null, // "New one"||"Existing one"
                    linkToPage: null,
                    processName: '',
                    advancedOptions: true,
                },
                // {
                //     fieldType: "Upload",
                //     group: "Upload",
                //     span: 7,
                //     text: "Upload",
                //     image: { show: null, saved: null },
                //     height: 21,
                //     isDisabled: null,
                //     guid: null
                // },
            ],
            sortElementOptions: {
                group: {
                    name: 'formbuilder',
                    pull: false,
                    put: true
                },
                sort: true
            },

            dropElementOptions: {
                group: {
                    name: 'formbuilder',
                    pull: 'clone',
                    put: false
                },
                sort: false,
                filter: ".is-disabled"
            }
        };
    },
    methods: {
        deleteElement(index, form) {
            vm.$store.commit('guides/setActiveField', []);
            vm.$store.commit('guides/setActiveTabForFields', 'elements')
            vm.$delete(form, index);
        },

        cloneElement(index, field, form) {
            var cloned = null;
            if (field.fieldType === 'Upload') {
                field.image.saved = false;
            }
            cloned = _.cloneDeep(field); // clone deep lodash
            form.splice(index, 0, cloned);
        },

        editElementProperties(field) {

            window.vm = vm;
            vm.$store.commit('guides/setActiveField', field);
            vm.$store.commit('guides/setActiveTabForFields', 'properties');

        }
    }
});
