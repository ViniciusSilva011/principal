import Vue from "vue";
import Router from "vue-router";

import store from "./store/store";
import axios from "axios";
import { verifyConditions } from '@/api/api-process'
import NProgress from "@/plugins/nprogress";

const Home = () => import("./views/guide/Home.vue");
const Preview = () => import("./views/guide/Preview.vue");
const Guide = () => import("./views/guide/Guide.vue");
// const GuideMenu = () => import("./views/guide/GuideMenu.vue");

Vue.use(Router);


axios.defaults.baseURL = store.state.guides.server;
const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/main_guides'
    },
    {
      path: "/preview",
      name: "preview",
      component: Preview
    },
    {
      path: '/guide/', component: Guide,
      children: [{
        path: ":id",
        name: "process",
        props: true,
        component: Home,

        beforeEnter(to, from, next) {
          verifyConditions(to.params.id)
            .then((resp) => {
              if (resp.hasOwnProperty('isUndefined') && resp.isUndefined) {
                let id = _.first(resp.pages).id;
                next(`/guide/${id}`);
              }
              else
                next();
            });
        },
      },
      ],
    },
    {
      path: '*',
      redirect: '/guide/1'
    },
  ]
});
router.beforeResolve((to, from, next) => {
  NProgress.start();
  next();
});

router.afterEach(() => {
  NProgress.done();
  store.commit('guides/setActiveTabForFields', "elements")
});
export default router;
