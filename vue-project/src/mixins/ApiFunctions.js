
export default {
  methods: {
    deleteImage() { //working on it
      return new Promise((resolve) => {
        this.$http({
          method: 'delete',
          url: '/api/images',
          data: this.activePage.temp.imagesFileToUpload,
          headers: { 'Content-Type': 'application/json' }
        }).then((r) => {
          resolve(r);
        })
      })
    },
    addImagesToDb(formDataObj) {
      for (let i = 0; i < this.activePage.temp.imagesFileToUpload.length; i++) {
        if (this.activePage.temp.imagesFileToUpload[i].file !== null)
          formDataObj.append('images_' + i, this.activePage.temp.imagesFileToUpload[i].file)
      }
    },
    deleteImagesFromServer(formDataObj) {
      formDataObj.append('deleteImagesFromServer', JSON.stringify(this.activePage.temp.deleteImagesFromServer))
    },
    updatePageIntoDB(guide) {
      return new Promise((resolve, reject) => {
        let dataToDB = new FormData();
        dataToDB.append('activePage', JSON.stringify(this.activePage));
        if (_.has(this.activePage, 'temp.imagesFileToUpload.length') && this.activePage.temp.imagesFileToUpload.length > 0)
          this.addImagesToDb(dataToDB);

        if (_.has(this.activePage, 'temp.deleteImagesFromServer.length') && this.activePage.temp.deleteImagesFromServer.length > 0)
          this.deleteImagesFromServer(dataToDB);

        this.$http({
          method: "post",
          url: `/api/guideUpdate/${this.activePage.id}`,
          'data': dataToDB,
          headers: { "Content-Type": "multipart/form-data" }
        })
          // eslint-disable-next-line no-unused-vars
          .then(({ data }) => {
            // this.syncModifications(data);
            resolve(guide);
          })
          .catch(({ data }) => {
            reject(data);
          });
      });
    },
    syncModifications(data) {
      //* sync pages
      let pages = _.cloneDeep(this.pages);
      let activePage = _.cloneDeep(this.activePage);

      let indexPageAct = _.findIndex(pages, e => e.id == activePage.id);
      pages[indexPageAct] = _.cloneDeep(activePage);

      this.setPages(pages);

      //* sync activePage
      let guideObjUpdated = _.assign(data, {
        temp: {
          deleteImagesFromServer: [],
          imagesFileToUpload: [],
          imagesToUpdate: []
        }
      });
      this.setActivePage(guideObjUpdated);
    }
  },
}