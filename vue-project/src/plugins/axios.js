import Vue from 'vue'
import axios from 'axios'
import store from '@/store/store';
Vue.use({
    install(Vue) {
        Vue.prototype.$http = axios.create({
            baseURL: store.state.guides.server,
            'X-Requested-With': 'XMLHttpRequest'
        })
        Vue.prototype.$http.interceptors.request.use(config => {
            return config
        }, error => Promise.reject(error))

        Vue.prototype.$http.interceptors.response.use(res => {
            return res
        }, error => Promise.reject(error))

    }
})