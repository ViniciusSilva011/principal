import 'nprogress/nprogress.css';
import Vue from 'vue';

const NProgress = require('nprogress/nprogress.js');
NProgress.configure({ minimum: 0.1, trickleSpeed: 150 });
NProgress.set(0.1);

Vue.use({
    install(Vue) {
        Vue.prototype.$nprogress = NProgress;
    }
})

export default NProgress;